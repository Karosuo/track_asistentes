/*
  Reading multiple RFID tags, simultaneously!
  By: Nathan Seidle @ SparkFun Electronics
  Date: October 3rd, 2016
  https://github.com/sparkfun/Simultaneous_RFID_Tag_Reader

  Every tag has a unique ID (the TID) that is not editable.
  There is also the chip vendor ID and model ID for the tag.

  TIDs are 20 bytes, 160 bits or 1,460,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 different possible tag IDs

  This example shows how to read the tag ID.
  * 
  * 
  * Modified by UABC STR team
  * Rafael Karosuo
  * David Gutierrez
*/

#include <SoftwareSerial.h> //Used for transmitting to the device
#define MAX_PEOPLE 150 ///< How much people will be able to read in one time cycle, if not defined 64 is default
#define CYCLE_TIME 1 ///< Number of seconds that will stay reading tags, all tags read will be part of the same group, after this time passes the group will be sent to PC to process, then it'll re-enter, if not defined, 5 secs is default

#include "uabc_rfid.h" ///< tools added by UABC team, small protocols or so, if wanted to define values, must be done before this declaration

SoftwareSerial softSerial(2, 3); //RX, TX

#include "SparkFun_UHF_RFID_Reader.h" //Library for controlling the M6E Nano module
RFID nano; //Create instance

unsigned long previousMillis = 0;        // will store last time 
const long interval = 1000 * CYCLE_TIME;           // interval (milliseconds)


void setup()
{
  Serial.begin(115200);

  while (!Serial);
  Serial.println();
  Serial.println("Initializing...");

  if (setupNano(38400) == false) //Configure nano to run at 38400bps
  {
    Serial.println("Module failed to respond. Please check wiring.");
    while (1); //Freeze!
  }

  nano.setRegion(REGION_NORTHAMERICA); //Set to North America

  nano.setReadPower(500); //5.00 dBm. Higher values may cause USB port to brown out
  //Max Read TX Power is 27.00 dBm and may cause temperature-limit throttling

  //nano.enableDebugging(); //Turns on commands sent to and heard from RFID module
}

void loop()
{
  unsigned long currentMillis = millis();
  previousMillis = currentMillis;

  while(currentMillis - previousMillis <= interval) {
    // save the last time you blinked the LED
    currentMillis = millis();
    //Serial.println(F("Get one tag near the reader. Press a key to read unique tag ID."));
    //while (!Serial.available()); //Wait for user to send a character
    //Serial.read(); //Throw away the user's character
    char ID[20];
    byte response;
    byte myTID[20]; //TIDs are 20 bytes
    byte tidLength = sizeof(myTID);
    
    //Read unique ID of tag
    response = nano.readTID(myTID, tidLength);
    if (response == RESPONSE_SUCCESS)
    {
      printTag(myTID,tidLength);
    }
    else{
     
     //Serial.println("Failed read");
    }
  }
  Serial.println("----------------------------------");
}

//Gracefully handles a reader that is already configured and already reading continuously
//Because Stream does not have a .begin() we have to do this outside the library
boolean setupNano(long baudRate)
{
  nano.begin(softSerial); //Tell the library to communicate over software serial port

  //Test to see if we are already connected to a module
  //This would be the case if the Arduino has been reprogrammed and the module has stayed powered
  softSerial.begin(baudRate); //For this test, assume module is already at our desired baud rate
  while(!softSerial); //Wait for port to open

  //About 200ms from power on the module will send its firmware version at 115200. We need to ignore this.
  while(softSerial.available()) softSerial.read();
  
  nano.getVersion();
  
  if (nano.msg[0] == ERROR_WRONG_OPCODE_RESPONSE)
  {
    //This happens if the baud rate is correct but the module is doing a ccontinuous read
    nano.stopReading();

    Serial.println(F("Module continuously reading. Asking it to stop..."));

    delay(1500);
  }
  else  
  {
    //The module did not respond so assume it's just been powered on and communicating at 115200bps
    softSerial.begin(115200); //Start software serial at 115200

    nano.setBaud(baudRate); //Tell the module to go to the chosen baud rate. Ignore the response msg

    softSerial.begin(baudRate); //Start the software serial port, this time at user's chosen baud rate
  }

  //Test the connection
  nano.getVersion();
  if (nano.msg[0] != ALL_GOOD) return (false); //Something is not right

  //The M6E has these settings no matter what
  nano.setTagProtocol(); //Set protocol to GEN2

  nano.setAntennaPort(); //Set TX/RX antenna ports to 1

  return (true); //We are ready to rock
}


