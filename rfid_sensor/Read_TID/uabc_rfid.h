//#ifndef UABC_RFID_H_
//#define UABC_RFID_H_
//#endif


#define ATMEGA_TYPE 328 ///< Indicates which uC is used, to use a smaller queue if needed, in order to use another uC, need to comment all that non used ones
//#define ATMEGA_TYPE 1280
//#define ATMEGA_TYPE 2560

#if ATMEGA_TYPE == 328
	#define MAX_QUEUE_LEN 800 ///< 800 bytes queue len, means 40 TID of 20bytes, since 328 has 2k sram
#elif ATMEGA_TYPE == 1280 || ATMEGA_TYPE == 2560
	#define MAX_QUEUE_LEN 3000 ///< 3k bytes, means the 150 TIDs that M6E can handle per second, 1280/2560 has 8k sram
#endif

/**
* CYCLE_TIME and TID_LENGTH could be user defined if needed, just taking care that if user didn't define'em they have a default value
*/
#if !defined(CYCLE_TIME)
	#define CYCLE_TIME 5 ///< Default cycle time, indicates how much time will be spent reading and saving, once it's over, it sends all what's saved over serial to PC, depends on the flag if pc is listeing or is talking
#endif
#if !defined(TID_LENGTH)
	#define TID_LENGTH 4 ///< Default TID 20 bytes as Gen UHF Tags
#endif

#define IS_EMPTY(buffer) (buffer.tail==buffer.head) ///< Macro that checks if queue is empty or not, uses C int bool logic, 0 false, non 0 true
#define IS_FULL(buffer) (((buffer.tail+1)%MAX_QUEUE_LEN) == buffer.head)///< Macro that checks if queue is full or not, uses C int bool logic, 0 false, non 0 true

/**
* Circular queue that holds the non repited rifid tags while the cycle_time isn't reached
* It uses fixed size double index array meaning that will be declared on .bss segment, so values copied in once the pointer head leaves them
* in next roud they'll be overwritten and no lost pointers will be left
* The logic of enqueue and dequeue is the same as if it was a byte element ring queue (+1), since the head/tail pointers are used
* in the MAX_QUEUE_LEN index so it jumps TID_LENGTH bytes and leaves the access to the queue transparent
*/
typedef struct {
	char buffer[MAX_QUEUE_LEN][TID_LENGTH]; ///< Buffer that holds the non repited rfid-tag ids, TIDs typcally are 20 bytes
	unsigned char tail; ///< Circular queue tail, the one that increments when new element comes in
	unsigned char head; ///< Circular queue head, the ones that increments when an element goes out	
} tag_buffer_queue_t;

unsigned int elementInQueue(tag_buffer_queue_t * ring_buffer, unsigned char * tid); ///< Check if tid param is already on ring_buffer, 1/true if it already exists otherwise returns 0/false
void enqueue(tag_buffer_queue_t * ring_buffer, unsigned char * tid); ///< Pushes the tid passed as parameter (copies it's values into the queue), affects the queue passed as param
void dequeue(tag_buffer_queue_t * ring_buffer, unsigned char * tid); ///< Pops an element from the queue received and copies it to the pointed space (tid param)
void printTag(unsigned char *TID, unsigned char tidLength); ///< Sends over serial the TID string received
void printQueue(); ///< Wrapper of the printTag function, pops everything to the queue and sends it through serial comm
tag_buffer_queue_t * getBufferPointer();