# README #

In this directory are all the rfid firmware files that needed to program the red board (arduino based board) and the Nano rfid reader.
Each subdir is an arduino project based on the examples provided with the Sparkfun Nano library.
Constant_Read and Read_EPC are just for reference while Read_TID is the main project for configuring readboard and Nano.
That's achieved with the help of some extra tools in the library called "uabc_rfid.h"

### What is this repository for? ###

* Keep versioning of the firmware for the redboard and the Nano rfid reader
 # Need to install the Arduino IDE
 # Add Sparkfun Nano library to the Arduino IDE (Nano Multiple RFID M6E from ThingMagic)
 # Follow the hookup guide of the Sparkfun product page

* 0.3
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
