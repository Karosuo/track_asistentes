
/**
*	@file uabc_rfid.h
*	@author Rafael Karosuo@UABC
*	@date 24 Apr 2017
* 	@brief It's a toolset to be the interface bt a PC and a Nano M6E multi rfid reader
*	it provides a ring queue structure to handle strings (tid elements), as well as functions to send'em over serial and check if already exist an element in the queue
* 	The serial communication is Arduino based, as well as the interrupt SerialEvent
*	If an arduino Mega or simil is being used, we could use serialEvent1-3
*/

#include <Arduino.h>

#include "uabc_rfid.h"

unsigned int pc_listening = 1; ///< Indicates if PC will always listen the serial port (value 1, or true) or if it starts the conversation asking values from queue (value 0, or false)
static tag_buffer_queue_t outgoing_buffer = {0}; ///< Instance of the queue type

/**
* Returns the available instance of the ring queue (outgoing_buffer)
* @param [out] tag_buffer_queue_t pointer of the outgoing_buffer
*/
tag_buffer_queue_t * getBufferPointer()
{
	return &outgoing_buffer;
}

/**
 * Genera dequeue function, pops out the head tid string from the ring queue
 * @param [in] ring_buffer is a pointer to the buffer from which will be poped the tid string
 * @param [in] tid is a pointer that indicates where's going to be copied the string poped from queue
 * ///< MUST ENSURE THAT THIS IS CALLED ONLY WHEN NOT EMPTY
 * */
void dequeue(tag_buffer_queue_t * ring_buffer, unsigned char * tid)
{
	if(ring_buffer->tail != ring_buffer->head) ///< Is not empty
	{
		int i; ///< Index to manage the tid 20 bytes
		for(i=0; i<TID_LENGTH; i++)
		{
			tid[i] = ring_buffer->buffer[ring_buffer->head][i]; ///< Copies the tid in head
		}
		ring_buffer->head += 1; ///< Increment head, to pop the element
	}//
}


/**
 * General enqueue function, that takes the ring buffer and inserts a char array as a new component
 * @param [in] ring_buffer is the pointer to the tag_buffer_queue that will receive the data
 * @param [in] data is the char pointer which will be used to get the string and put it into the queue
 * ///< MUST ENSURE THAT THIS IS CALLED ONLY WHEN NOT FULL
 * */
void enqueue(tag_buffer_queue_t * ring_buffer, unsigned char * tid)
{
	if(((ring_buffer->tail+1)%MAX_QUEUE_LEN) != ring_buffer->head) ///< If not full
	{
		int i; ///< Index for the TID array in queue tail position
		for(i=0; i<TID_LENGTH; i++) ///<TID array in Gen2 UHF typically is 20 bytes
		{
			ring_buffer->buffer[ring_buffer->tail][i] = tid[i];
		}
		ring_buffer->tail = (ring_buffer->tail+1) % MAX_QUEUE_LEN; ///< Go to the next position, avoid overlap
	}//
}


/**
* Sends over serial a char array, it's used to send the TID tag over serial comm
* Uses the Serial Arduino class, and implies that was configured before calling printTag
* @param [in] TID uchar pointer where the TID is located
* @param [in] tidLength the length of the array
*/
void printTag(unsigned char *TID, unsigned char tidLength)
{
    ///< Enable use of Serial functions in other files, this a referencie of the Serial instance on the main file
    unsigned char x;
    for(x = 0 ; x < tidLength ; x++)
      {
        if(TID[x] < 0x10) Serial.print("0");
        Serial.print(TID[x], HEX);
      }
      Serial.println();
}


/**
 * Pops everything from the queue and prints it over serial using printTag
 * It's a wrapper functions and depends on:
 *      printTag() and dequeue()
*/
void printQueue()
{
    unsigned char temp_tid[TID_LENGTH]; ///< Holds each TID befor printing   
    while(!IS_EMPTY(outgoing_buffer))
    {
      dequeue(&outgoing_buffer, temp_tid);///< Gets the head tid
      printTag(temp_tid, TID_LENGTH); ///< Sends over serial that TID
    }
}

/**
* Determines if a string element is already in the queue or not
* Compartes from the lower byte, one by one.
* If a byte in the comparison is different, it jumps to the next element or returns 0/false (if it's the last), meaning that the element wasn't already in the queue
* If 
* @param [in] ring_buffer, is the queue to be fetched to check if the element is already there, is a queue pointer to let this function work indpendently with any ring buffer
* @param [in] tid, is the element to be searched, as in this case is a string it is a char pointer
*/
unsigned int elementInQueue(tag_buffer_queue_t * ring_buffer, unsigned char * tid)
{
	unsigned char tmp_index = ring_buffer->head; ///< Will be the temp head pointer to go over the queue without poping out elements
	unsigned char  tid_index; ///< Index to go alog the tid bytes comparison
	while(tmp_index != ring_buffer->tail)
	{
		for(tid_index=TID_LENGTH; tid_index>0; tid_index--) ///< Go in reverse order, to comparte low bytes first
		{
			if(ring_buffer->buffer[tmp_index][tid_index-1] != tid[tid_index-1])
				break; ///< if different byte means it's different, no need to end the comparison, check the next tid
								
		}//end for tid_index
		if (tid_index == 0) ///< If the last TID needed to go al way through, means all bytes are equal, so it was already in the queue
			return 1;
		tmp_index++;
	}

	return 0; ///< if no tid needed to check all of it's bytes, means that all were different, so wasn't in the queue
}
