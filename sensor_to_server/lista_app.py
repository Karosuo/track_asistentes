from Tkinter import *
from PIL import ImageTk, Image
from io import BytesIO
import base64
from serial import SerialException
import serial
import time
import urllib2
import json
import Queue
import threading

## Loop to keep reading serial port with the port and baudrate given.
# @param  port Port to hear
# @param  Baudrate speed in bauds
# @return None
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def serial_loop(port,baudrate):
    print '--> Serial loop thread started\n\r'
    while True:
        try:
            print 'opening port',port,'...'
            ser = serial.Serial(port,baudrate)
            print 'Ready'

            while True:
                while ser.inWaiting() >0:
                    data = ser.readline()
                    data = data.strip('\r\n')
                    print 'readed data:',data
                    if data  in assistant_views_dic:
                        pass
                    else:
                        request_queue.join() 
                        request_queue.put(data)
                        request_queue.task_done()
                        print data,' Adeed to queue'
        except SerialException:
            print "No connection with the device could be established, retrying in 1 second..."
            time.sleep(1)
        except IOError:
            print "Port is disconected"
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Loop to keep making requests to server while the queue is not empty.
# @param  None
# @return None
def request_loop():
    print '--> Request check loop thread started\n\r'
    while True:
        if request_queue.empty():
            pass
        else:
            item = request_queue.get()
            response=request(item)
            responses_queue.put(response)
    print 'Thread stopped '

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to perform a request to server with a RFID given
# @param  rfid Key to perform a request in the server DB
# @return response Assitant related to key returned by the server
def request(rfid):
    url = "http://127.0.0.1:8000/json_rest/"
    response = None
    dic = {"rfid":rfid}
    try:
        data = json.dumps(dic)
        r = urllib2.urlopen(url,data)
        response = r.read()
        r.close()
        response=json.loads(response)
        # Add RFID readed to response dictionary
        response['rfid'] = rfid

        print response
    except ValueError, err:
        print "Something happened! Loading JSON from server", err
    except urllib2.HTTPError, err:
        print "Something happened! Error code", err.code
    except urllib2.URLError, err:
        print "Some other error happened:", err.reason
    return response

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to update gui scroll region while more assistants are painted
# @param  None
# @return None
def update_scroll_region():
    # Necessary to update the scrollbar as long new frames are added
    canvas.configure(scrollregion=canvas.bbox('all'))

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to add a view with assitant info requested from server on the gui
# @param  frame Main gui reference
# @return None
def addView(frame):
    global ROWS,COLS,main_frame
    if COLS == 2 :
        COLS = 0
        ROWS = ROWS + 1
        #main_frame.rowconfigure(ROWS, weight=1)
    #print ROWS,COLS
    #main_frame.rowconfigure(ROWS, weight=1)
	#main_frame.columnconfigure(COLS, weight=1)

    frame.grid(row=ROWS,column=COLS,sticky=N+S+W+E)
    canvas.configure(scrollregion=canvas.bbox('all'))
    COLS = COLS + 1


# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to update the size of the scroll region in the main gui
# @param  None
# @return None
def on_configure(event):
    # update scrollregion after starting 'mainloop'
    # when all widgets are in canvas
    canvas.configure(scrollregion=canvas.bbox('all'))

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to send a view to main gui and save it in a list to keep a reference
# @param  dictionary Dictioanry with the assistant's information
# @return new_frame View with the assistant information
def create_assistant_view(dictionary = None):
    global views_list
    print "Creating new assistant view"
    new_frame = createView(main_frame,dictionary)
    addView(new_frame)
    #views_list.append(new_frame)
    return new_frame
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to check if the response queue has assistants to paint
# @param  None
# @return new_frame None
def check_responses():
    #print "Checking responses,",responses_queue.qsize()
    if responses_queue.empty():
        pass
    else:
        item = responses_queue.get()
        gotRFID=item['rfid']
        if gotRFID in assistant_views_dic:
            print "This Assistant is already showed"
        else:
            view=create_assistant_view(item)
            assistant_views_dic[gotRFID] = view

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to perform background tasks in other Thread
# @param  None
# @return new_frame None
def backgroundTasks():
    check_responses()
    update_scroll_region()
    app.after(delay,backgroundTasks)
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to create a view 
# @param  dictionary Dictionary with the information for the view
# @return new_frame View with the information
def createView(parent,dictionary = None):
    global error_codes
    frame = Frame(parent,bg='white',width=w/2,height=h/2,borderwidth=2, relief="groove",padx = 30, pady= 30)
    container = Frame(frame,bg='white')
    container.place(relx=0.5, rely=0.5, anchor=CENTER)

    if 'error' in dictionary:
        code = int(dictionary['error'])
        error_message = error_codes[code]+" - RFID:"+dictionary['rfid']
        frame.config(bg = "white")

        canvas = Canvas(container, bg="white", width=200, height=200,highlightthickness=0)
        canvas.pack()
        canvas.create_image(100, 100, image=error_image)
        Label(container,text='ERROR!',fg = "red",font= "Helvetica 16 bold ",bg='white').pack()
        Label(container,text=error_message,fg = "red",font= "Helvetica 12",bg='white').pack()

    else:
        assistant_rfid=dictionary['rfid']
        assistant_id =dictionary['id']
        assistant_name =dictionary['nombre']
        assistant_faculty=dictionary['facultad']
        assistant_position=dictionary['cargo']
        assistant_substitute=dictionary['suplente']
        assistant_start_date =dictionary['fecha_inicio']

        if assistant_substitute == '0':
            assistant_substitute = 'No'
        else:
            assistant_substitute = 'Si'
        Label(container,text="RFID:",font= "Helvetica 10 bold ",bg='white',anchor=W).grid(row=0,column=0,sticky="nsew",padx=5)
        Label(container,text="Matricula:",font= "Helvetica 10 bold ",bg='white',anchor=W).grid(row=1,column=0,sticky="nsew",padx=5)
        Label(container,text="Nombre:",font= "Helvetica 10 bold ",bg='white',anchor=W).grid(row=2,column=0,sticky="nsew",padx=5)
        Label(container,text="Facultad:",font= "Helvetica 10 bold ",bg='white',anchor=W).grid(row=3,column=0,sticky="nsew",padx=5)
        Label(container,text="Cargo:",font= "Helvetica 10 bold ",bg='white',anchor=W).grid(row=4,column=0,sticky="nsew",padx=5)
        Label(container,text="Suplente:",font= "Helvetica 10 bold ",bg='white',anchor=W).grid(row=5,column=0,sticky="nsew",padx=5)
        Label(container,text="Fecha de inicio:",font= "Helvetica 10 bold ",bg="white",anchor=W).grid(row=6,column=0,sticky="nsew",padx=5)
        l1=Label(container,text="RFID",font= "Helvetica 10  ",bg='white',anchor=W,borderwidth=1, relief="sunken",width=25).grid(row=0,column=1,sticky="ew",padx=5)
        l2=Label(container,text=assistant_id,font= "Helvetica 10  ",bg='white',anchor=W,borderwidth=1, relief="sunken").grid(row=1,column=1,sticky="ew",padx=5)
        l3=Label(container,text=assistant_name,font= "Helvetica 10  ",bg='white',anchor=W,borderwidth=1, relief="sunken").grid(row=2,column=1,sticky="ew",padx=5)
        l4=Label(container,text=assistant_faculty,font= "Helvetica 10  ",bg='white',anchor=W,borderwidth=1, relief="sunken").grid(row=3,column=1,sticky="ew",padx=5)
        l4=Label(container,text=assistant_position,font= "Helvetica 10  ",bg='white',anchor=W,borderwidth=1, relief="sunken").grid(row=4,column=1,sticky="ew",padx=5)
        l6=Label(container,text=assistant_substitute,font= "Helvetica 10  ",bg='white',anchor=W,borderwidth=1, relief="sunken").grid(row=5,column=1,sticky="ew",padx=5)
        l7=Label(container,text=assistant_start_date,font= "Helvetica 10  ",bg='white',anchor=W,borderwidth=1, relief="sunken").grid(row=6,column=1,sticky="ew",padx=5)
        try:
            image = None
            if 'image_encoded' in dictionary:
                assistant_image_encoded = dictionary['image_encoded']
                image=Image.open(BytesIO(base64.b64decode(assistant_image_encoded)))
                image = image.resize((300, 300), Image.BILINEAR)
                image = ImageTk.PhotoImage(image)
                images.append(image)
            else:
                image = no_image
            label = Label(container, image=image,borderwidth=2, relief="solid")
            label.grid(row=0,column=2,rowspan = 7)
        except Exception as e:
            print e
    return frame
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Function to clean all views in the main gui and the list with their references
# @param  None
# @return None
def clear():
    global images,ROWS,COLS,assistant_views_dic
    for i in assistant_views_dic:
        assistant_views_dic[i].destroy()
    assistant_views_dic.clear()
    del images[:]
    ROWS = 0
    COLS = 0
    print 'images:',len(images),'views:',len(assistant_views_dic)
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Golbal variables
## Error codes from server
error_codes = {
    0:'El servidor no respondio nada',
    1:'No hay ningun asistente asociado',
    2:'No hay ningun evento programado para hoy',
    3:'Otro error'}
## delay to execute tasks in background
delay = 100
## port to hear
PORT = 'COM2'
## default speed in bauds
BAUDRATE = 115200
## Rows and cols for the grid in main gui
ROWS = 0
COLS = 0
## List to keep references for every image on the assitants's views
images=[]
## Dictionary wiht references to all assistant's views on the main gui
assistant_views_dic = {}
## queue with response from server (assistant's information)
responses_queue = Queue.Queue()
## Queue with the rfids to perform requests to server
request_queue = Queue.Queue()
## Default image in case of no one image was received from server in the assistant information
no_image=Image.open("res/no-photo-available.png").convert("RGBA")
## Default image in case of server error
error_image = Image.open("res/error.png").convert("RGBA")
## Main
if __name__=='__main__':

    print 'Number of arguments:', len(sys.argv), 'arguments.'
    print 'Argument List:', str(sys.argv)

    if len(sys.argv) == 1:
        print 'No parameters found'
    else:
        PORT = sys.argv[1]
        # Starts the request check loop thread
        REQUEST_THREAD = threading.Thread(target=request_loop)
        REQUEST_THREAD.daemon = True
        REQUEST_THREAD.start()

        # Starts the serial read loop thread
        SERIAL_THREAD = threading.Thread(target=serial_loop, args=(PORT,BAUDRATE,))
        SERIAL_THREAD.daemon = True
        SERIAL_THREAD.start()

        ## Main gui
        app = Tk()
        tk_rgb = "#%02x%02x%02x" % (1, 146, 69)
        no_image=ImageTk.PhotoImage(no_image)
        error_image= ImageTk.PhotoImage(error_image)
        h = app.winfo_screenheight()
        w = app.winfo_screenwidth()
        app.wm_state('zoomed')
        app.title("Lista App")
        head_frame = Frame(bg=tk_rgb)
        head_frame.pack(side=TOP, fill="x", expand=True)
        Label(head_frame,text="Lista de Asistentes",font= "Calibri 24 bold ",fg="white",bg=tk_rgb).pack(side=LEFT)
        reset = Button(head_frame,fg="white",bg=tk_rgb,text="Limpiar",font= "Calibri 12 bold  ", command = clear )
        reset.pack(side = RIGHT,padx = 10)
        # --- create canvas with scrollbar ---
        canvas = Canvas(app,height=h,width=w-20)
        canvas.pack(side=LEFT, fill="both", expand=True)
        scrollbar = Scrollbar(app, orient=VERTICAL, command=canvas.yview)
        scrollbar.pack(side=LEFT, fill='y', expand = True)
        canvas.configure(yscrollcommand = scrollbar.set)
        # when all widgets are in canvas
        canvas.bind('<Configure>', on_configure)
        # --- put frame in canvas ---
        main_frame = Frame(canvas)
        canvas.create_window((0,0), window=main_frame, anchor='nw')
        app.after(delay,backgroundTasks)
        app.mainloop()
