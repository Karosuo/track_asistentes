from subprocess import call
from subprocess import Popen, CREATE_NEW_CONSOLE
import serial
import sys
import time


## This function executes and infitine loop, which will read the serial port
# get the RFID and put it wherever the focus is
# @param port Serial Port to read
# @return None
def serial_loop():
	print '--> Serial loop thread started\n\r'
	while True:
		try:
			print 'Opening port',PORT,'...'
			ser = serial.Serial(PORT,BAUDRATE)
			print 'Waiting for data...'
			while True:
				while ser.inWaiting() >0:
					data = ser.readline()
					data = data.strip('\r\n')
					print 'Data Readed {} - {}\r\n'.format(data,time.strftime("%H:%M:%S"))
					call(["kb_stroke_sim/vkb_w32.exe", data+'\n'])
					count = 5
					while count >= 0:
						time.sleep(1)
						print 'Ready in  {} seconds\r'.format(count),
						count = count - 1
					print 'Waiting for data...'
					ser.flushInput()  #flush input buffer, discarding all its contents
					ser.flushOutput() #flush output buffer, aborting current output
		except Exception as e:
			print "ERROR INFO: ",e
			count = 5
			while count >= 0:
				time.sleep(1)
				print 'Retry in  {} seconds\r'.format(count),
				count = count - 1
			pass

PORT = 'com1'
BAUDRATE = 115200

## Main
if __name__=='__main__':
	print 'Number of arguments:', len(sys.argv), 'arguments.'
	print 'Argument List:', str(sys.argv)

	if len(sys.argv) == 0:
		print 'No parameters found'
	else:
		PORT = sys.argv[1]
		serial_loop()