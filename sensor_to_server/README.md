### Requirements ###

* Python 2.7.13
* [download](https://www.python.org/downloads/)
* Make sure that python is in the system PATH
* pySerial Library
* PIL Library // from the pillow Library: PIL is basically dead. Pillow is a maintained fork of PIL.
* 			 // pip install pillow

### How to install ###

For example:
```sh
$ pip install pySerial
```

### How to run the app ###
```sh
$ python main.py
```

### instructions ###

The main aplicattion shows two button "lista","registro", and combo box with availables serial ports, only select a port, click button to launch any of two apps and that's all
* lista_app.py script
Reads the serial port in order to get the RFID then perform a request to server with it, and shows the assistan information related to rfid in a window
* Aplication 2
Reads the serial port in order to get whatever reads and then paste it in whatever focus is

* Notes
Only one app can be launched at time, when launches an app and the other is 
still running automatically will be closed in order to open the other.
