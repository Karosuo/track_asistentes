from subprocess import call
from subprocess import Popen, CREATE_NEW_CONSOLE
from Tkinter import *
import ttk
import sys
import serial
import subprocess

## This function scan serials ports and shows which found.
# @param num_ports Range of ports
# @param verbose Flag to print information
# @return dispositivos_serie List with every port found
def Scan(num_ports = 20, verbose = False):
	dispositivos_serie = []
	if verbose:
		print "Escaneado %d puertos serie" % num_ports
	for i in range(num_ports):
		if verbose:
			sys.stdout.write("puerto %d: " % i)
			sys.stdout.flush()
		try:
			s = serial.Serial('COM'+str(i))
			if verbose: print "OK --> %s" % s.portstr
			dispositivos_serie.append( s.portstr)
			s.close()
		except:
			if verbose: print "NO"
			pass
	return dispositivos_serie


## This function launchs the focus_app.py script
# @param port Serial port that will be open by the script
# @return None
def registro(port):
	global proc
	if proc != None:
		proc.terminate()
	proc = Popen(['python',"focus_app.py", port],shell = False,creationflags=CREATE_NEW_CONSOLE)

## This function launchs the listar_app.py script
# @param port Serial port that will be open by the script
# @return None
def lista(port):
	global proc
	if proc != None:
		proc.terminate()
	proc = Popen(['python',"lista_app.py", port],shell = False,creationflags=CREATE_NEW_CONSOLE)

## This function executes when use closes the menu app,
# when in happens also will be closed the script
# @param None
# @return None
def on_exit():
	global proc,root
	if proc != None:
		proc.terminate()
	root.destroy()


# Color for background
tk_rgb = "#%02x%02x%02x" % (1, 146, 69)

# Script process id
proc = None
# Main window with the menu
root = Tk()


## Main gui
root.geometry("500x300")
root.title("Lanzador")
root.resizable(width = 0, height = 0)
root.wm_protocol("WM_DELETE_WINDOW", on_exit)


head_frame = Frame(bg = "white",height = 50)
head_frame.pack(fill = X, expand = True)
head_frame.pack_propagate(False)
Label(head_frame,text="Track Asistentes Menu",font= "Calibri 12 bold ",fg = tk_rgb,bg ="white").pack(pady = 5,padx = 20,side=LEFT)
Button(head_frame,text="INFO",font= "Calibri 10",fg = tk_rgb,bg ="white").pack(side=RIGHT,pady = 5,padx = 20)


registro_app_info = "Esta aplicacion recoge el RFID del asistente"
lista_app_info = "Esta aplicacion se encarga de tomar lista de los asistentes"
center_frame = Frame(bg = tk_rgb,height = 450,borderwidth=2, relief="solid")
center_frame.pack(fill = X)
center_frame.grid_propagate(False)

b1=Button(center_frame,width = 20,text="LISTA",font= "Calibri 11",fg = tk_rgb,bg ="white",state ="disabled",command = lambda: lista(ports.get()) )
b1.grid(row=0,column=0,pady = 20,padx = 20)
T1=Text(center_frame,height=2,width=30,font= "Calibri 11  ",bg =tk_rgb,fg = "white", relief="flat",wrap = "word")
T1.insert(END, lista_app_info)
T1.config(state ="disabled")
T1.grid(row=0,column=1)

b2=Button(center_frame,width = 20,text="REGISTRO",font= "Calibri 11",fg = tk_rgb,bg ="white",state ="disabled",command = lambda: registro(ports.get()))
b2.grid(row=1,column=0)
T2=Text(center_frame,height=2,width=30,font= "Calibri 11  ",bg =tk_rgb,fg = "white" ,relief="flat",wrap = "word")
T2.insert(END, registro_app_info)
T2.config(state ="disabled")
T2.grid(row=1,column=1)


config_info = "Selecciona el puerto correspondiente."
T3=Text(center_frame,height=2,width=50,font= "Calibri 11  ",bg =tk_rgb,fg = "white" ,relief="flat", wrap = "word")
T3.insert(END, config_info)
T3.config(state ="disabled")
T3.grid(row=3,column=0,columnspan = 2,sticky=W,padx = 20,pady=10)

# List with scanned ports
scannedPorts = Scan()
ports = ttk.Combobox(center_frame, text="ports", state='readonly')
if len(scannedPorts) > 0 :
	ports.config(values=scannedPorts)
	ports.current(0)
	b1.config(state ="normal")
	b2.config(state ="normal")
else:
	T4=Text(center_frame,height=2,width=50,font= "Calibri 11  bold ",bg =tk_rgb,fg = "red" ,relief="flat", wrap = "word")
	T4.insert(END, "No se detecto ningun puerto!")
	T4.config(state ="disabled")
	T4.grid(row=4,column=0,columnspan = 2,sticky=W,padx = 20,pady=10)


ports.grid(row=4,column=0,padx = 20,columnspan = 2,sticky=W)


root.mainloop()