/*DROP DATABASE consejo_db;*/
CREATE DATABASE consejo_db;
USE consejo_db;


CREATE TABLE Consejeros(
	id INT PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    facultad VARCHAR(50) NOT NULL,
    cargo VARCHAR(50) NOT NULL,
    image_path VARCHAR(100) NOT NULL,
    barcode_path VARCHAR(100) NOT NULL,
    suplente BOOLEAN,
    fecha_inicio DATETIME
);

CREATE TABLE RFID(
	id_RFID VARCHAR(60) PRIMARY KEY,
    id_consejero INT,
	FOREIGN KEY (id_consejero) REFERENCES Consejeros(id)
);

CREATE TABLE Evento(
	numero_evento INT PRIMARY KEY AUTO_INCREMENT,
	fecha DATETIME NOT NULL,
	lugar VARCHAR(50) NOT NULL
);

CREATE TABLE Consejero_Evento(
	timestamp DATETIME NOT NULL,
	id_consejero INT,
    numero_evento INT,
	FOREIGN KEY (id_consejero) REFERENCES Consejeros(id),
	FOREIGN KEY (numero_evento) REFERENCES Evento(numero_evento)
);
