insert into Consejeros(id, nombre, facultad, cargo, image_path, barcode_path, suplente, fecha_inicio)
select 0000283114,'Yail Rafael','FCQI','Maestro','./images/yail.png', './images/barcode00.svg',false, '2017/01/01' union all
select 0001200468,'Rafael Gomez','FCH','Director','./images/rafael.png', './images/barcode01.svg', false, '2017/01/01' union all
select 0001216912,'Ulices Ramirez','FMNI','Alumno','./images/ulices.png', './images/barcode02.svg', false, '2017/01/01';

insert into RFID(id_RFID, id_consejero)
select 'E20051860107019126600B72', 0001216912 union all
select 'E20051860107019126600B75', 0001200468;


insert into Evento(numero_evento, fecha, lugar)
select 1,'2017/03/10','Campus Otay' union all
select 2,'2017/04/11','Campus Ensenada' union all
select 3,'2018/02/08','Campus Otay';

/*SELECT id, nombre FROM Consejeros inner join RFID on Consejeros.id=RFID.id_consejero;*/