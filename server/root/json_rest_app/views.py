## @package json_rest_app
# This is the controller for the desktop app and server interaction.
#
# This view is responsible for responding to the desktop application with the requested queries
# using the RFID
from django.shortcuts import render
import json
import base64
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from io import BytesIO
from json_rest_app.models import ConsejeroEvento
from json_rest_app.models import Consejeros
from json_rest_app.models import Evento
from json_rest_app.models import Rfid
from django.core.files.storage import FileSystemStorage
from django.core import serializers
from django.utils import timezone

# This variable is used to represent a diferents errors
error_code = 0
# This variable is used to allow messages related to methods messages
verbose = True
# This tag is required to answer to unknown POST request
@csrf_exempt
## This function is responsible for verifying and receiving POST requests with RFID
# from the desktop app and answer back with te requested information
# @param request The POST object received from desktop app.
# @return response_json The response with requested infromation or None if something happens
def json_rest(request):
	# Check if the request is POST
	if request.method == "POST":
		print "\n\n---------- Processing request (JSON REST app) ----------"
		response =  process(request)
		if response == False:
			response = {"error":error_code}

			return HttpResponse(json.dumps(response))
		else:
			return HttpResponse(response)
	if request.method == "GET":
		HttpResponse("Oops! You seem lost, you shouldn't be here")


## This function is responsible for make all data process and query process
# @param request The POST object received from HTML form.
# @return response/False Return response json object if all is correct or False flag if something fail
def process(request):
	received = json_loads_request(request.body)
	if received == False:
		return False
	assistant = queryByRFID(received)
	if assistant == False:
		error_code = 1
		return False
	event = IsEventToday()
	if event == False:
		return False
	else:
		if assistance(assistant,event) == False:
			return False

	response = prepare_response(assistant)
	if response == False:
		return False
	else:
		return response


## This function shows debug info from functions
# @param function_name Name of calling function
# @param tag Tag 'ERROR','INFO','EXCEPTION'
# @param message Info Message
# @return None
def debug_info(function_name,tag, message):
    if verbose:
        print "---------------------------------------------------------------"
        print tag,'in function: ___',function_name,'()___'
        print '\t',tag,':',message
        print "---------------------------------------------------------------"


## This function gets the rfid in the post request object
# @param post_request The POST object received from client app.
# @return rfid/False Return RFID if all is correct or False flag if something fail
def json_loads_request(post_request):
	try:
		json_request_data = json.loads(post_request)
		debug_info('json_loads_request','INFO','Data received JSON from client: '+json_request_data['rfid'])
		return json_request_data['rfid']
	except Exception as e:
		debug_info('json_loads_request','EXCEPTION CAUGHT',e)
		return False


## This function checks if exists an assitant related to a rfid given
# @param rfid RFID to look in database
# @return assistant/False Return assitant object if all is correct or False flag if something fail
# Note: query Modelo.objects.get raises an exception if not found nothing
def queryByRFID(rfid):
	try:
		assistant = Consejeros.objects.get(rfid = rfid)
		debug_info('queryByRFID','INFO','Recovered Assistant: '+str(assistant.id)+','+str(assistant.nombre)+','+str(assistant.facultad)+','+str(assistant.cargo)+','+str(assistant.image_path)+','+str(assistant.barcode_path)+','+str(assistant.suplente)+','+str(assistant.fecha_inicio))
		return assistant
	except Exception as e:
		debug_info('queryByRFID','EXCEPTION CAUGHT',e)
		return False



## This function checks if exists an event programed on the actual date
# @param None
# @return event_today/False Return event object if all is programed someone or False flag if something fail or not is programed
def IsEventToday():
	actual_date = timezone.now()
	actual_date = actual_date.strftime("%Y-%m-%d 00:00:00")
	try :
		event_today =  Evento.objects.get(fecha = actual_date)
		debug_info('IsEventToday','INFO','Recovered Event:' + str(event_today.numero_evento)+','+str(event_today.fecha)+','+str(event_today.lugar))
		return event_today
	except Exception as e:
		debug_info('IsEventToday','EXCEPTION CAUGHT',e)
		return False

## This function register an assistance
# @param assistant Assistan object
# @param event Event object
# @return event_today/False Return event object if all is programed someone or False flag if something fail or not is programed
def assistance(assistant,event):
	from django.db import connection
	try:
		cursor = connection.cursor()
		rowcount = cursor.execute('SELECT * FROM consejero_evento WHERE numero_evento = %s AND id_consejero = %s', [event.numero_evento,assistant.id])
		if rowcount > 0:
			debug_info('assistance','INFO','Assistence Not Registered')
		else:
			timestamp = timezone.now()
			timestamp = timestamp.strftime("%Y-%m-%d %H:%M:%S")
			register = ConsejeroEvento(id_consejero = assistant,numero_evento = event, timestamp = timestamp)
			register.save()
			debug_info('assistance','INFO','Assistence Registered')
	except Exception as e:
		debug_info('assistance','EXCEPTION CAUGHT',e)
		return False

## This function creates a json with the data of the assistant found
# @param assistant_queryset Assistan object
# @return response/False Return response json object if all is correct or False flag if something fail
def prepare_response(assistant_queryset):
	response = {
		"id": str(assistant_queryset.id),
		"nombre":str(assistant_queryset.nombre),
		"facultad":str(assistant_queryset.facultad),
		"cargo":str(assistant_queryset.cargo),
		"suplente":str(assistant_queryset.suplente),
		"fecha_inicio":str(assistant_queryset.fecha_inicio),
		}
	try:
		assistant_image = base64.b64encode(open(str(assistant_queryset.image_path), "rb").read())
		response['image_encoded'] = assistant_image
		
	except Exception as e:
		debug_info('prepare_response','EXCEPTION CAUGHT',e)

	try:
		response = json.dumps(response)
		debug_info('prepare_response','INFO','Response created SUCCESSFULLY')
		return response
	except Exception as e:
		debug_info('prepare_response','EXCEPTION CAUGHT',e)
		return False


# Method used only for test purposes
def test(request):
	from django.db import connection
	try:
		id_consejero = 12345678
		numero_evento = 1
		cursor = connection.cursor()
		rowcount = cursor.execute('SELECT * FROM consejero_evento WHERE numero_evento = %s AND id_consejero = %s', [numero_evento,id_consejero])
		row = cursor.fetchone()
		print rowcount
	except Exception as e:
		print e
	return HttpResponse('test')