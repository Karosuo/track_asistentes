## @package altas__app
# This is the controller for the Altas asistentes HTML page.
#
# This view is responsible for responding to formulary in the HTML page altas asistentes
from django.shortcuts import render
import json
import base64
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from io import BytesIO
from altas_app.models import Consejeros
from altas_app.models import Rfid
from django.core.files.storage import FileSystemStorage
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import AltaForm
import barcode
import os 



# This variable is used to allow messages related to functions debug information
verbose = True



## Tag to login required to access this view
@login_required
## This function is responsible for verifying and receiving POST requests
# from the HTML form and obtain the information, validate it
# and save it in the database.
# @param request The POST object received from HTML form.
# @return template The template with the response HTML form
def alta_form(request):
    if request.method == "POST":
        if 'submit-form1' in request.POST:
            process(request)
        if 'submit-form2' in request.POST:
            upload_multiple_files(request)  
    return render(request,'altas_app_templates/alta.html')




## This function shows debug info from functions
# @param function_name Name of calling function
# @param tag Tag 'ERROR','INFO','EXCEPTION'
# @param message Info Message
# @return None
def debug_info(function_name,tag, message):
    if verbose:
        print "---------------------------------------------------------------"
        print tag,'in function: ___',function_name,'()___'
        print '\t',tag,':',message
        print "---------------------------------------------------------------"



## This function is responsible for make all data process and save process
# for formulary data
# @param request The POST object received from HTML form.
# @return True/False Return True flag if all is correct or False flag if something fail
def process(request):
    print "\n\n---------- Processing request (ALTAS app) ----------"
    values = get_post_data(request)

    if values == False:
        messages.error(request, 'ERROR: Algo salio mal :(')
        return False

    if isAssistantAlreadyRegistered(values['rfid']) == True:
        messages.error(request, 'ERROR: Este asistente ya esta registrado')
        return False

    values['image_path'] = handle_uploaded_file(request.FILES.get('imageToUpload'),values['id'])
    if values['image_path'] == False:
        messages.error(request, 'ERROR: Formato de imagen no admitido\nFormatos validos: JPEG,PNG')
        return False

    values['barcode_path'] = create_barcode(values['id'])
    if values['barcode_path'] == False:
        messages.error(request, 'ERROR: No se pudo crear el codigo de barras')
        return False

    if CheckEmptyFields(values):
        messages.error(request, 'ERROR: No deben haber campos sin llenar')
        return False

    if save_assistant(values) == False:
        messages.error(request, 'ERROR: No se pudo registrar al usuario')
    else:
        messages.success(request, 'EXITO: Asistente registrado con exito')
        return True


## This function checks if the asisstan is already registered in database
# using rfid as a key
# @param _rfid The assistant's rfid
# @return True/False Retrun True flag if all if is found or False flag if not or something fail
def isAssistantAlreadyRegistered(_rfid):
    try:
        if Consejeros.objects.filter(rfid=_rfid).exists():
            debug_info('isAssistantAlreadyRegistered','INFO','Already registered')
            return True
        else:
            return False
    except Exception as e:
        debug_info('isAssistantAlreadyRegistered','EXCEPTION CAUGHT',e)
        return False  


## This function uploads multiple imagest to media/upload_images folder
# @param request The POST object received from HTML form.
# @return True/False Returns True flag if success or False flag if something goes wrong
def upload_multiple_files(request):
    try:
        files = request.FILES.getlist('fileToUpload')
        for file in files:
            filename=file.name.split('.')[0] 
            path = handle_uploaded_file(file,filename)
            if path == False:
                raise Exception("Algunas imagenes no se pudieron subir")
    except Exception as e:
        debug_info('upload_multiple_files','EXCEPTION CAUGHT',e)
        return False      

## This function gets the request POST object from the form
# and process it in order to get the data related to an assistant that
# will be register in the database
# @param request The POST object received from HTML form.
# @return Values/False Returns a dictionary with post data if success or False flag if something goes wrong
def get_post_data(request):
    try:
        values = {}
        values['rfid'] = request.POST.get('rfid')
        values['id'] = request.POST.get('id')
        values['nombre'] = request.POST.get('nombre')
        values['facultad'] = request.POST.get('facultad')
        values['cargo'] = request.POST.get('cargo')
        values['image_path'] = None
        values['barcode_path'] = None
        if request.POST.get('suplente') == 'true':
            values['suplente'] = True
        else:
           values['suplente'] = False 
        debug_info('get_post_data','INFO','Returned dict:'+str(values))
        return values
    except Exception as e:
        debug_info('get_post_data','EXCEPTION CAUGHT',e)
        return False

## This function creates a bar code on base an assistan id
# @param code_bar_name The name that will be assigned to the bar code image
# @return barpath/False Returns barcode's image path if success or None if something goes wrong
def create_barcode(code_bar_name):
    try:
        values = {}
        path = 'media/barcodes/'
        codegen = barcode.Code39(code_bar_name, writer=None, add_checksum=False)
        barpath = codegen.save(path+code_bar_name)  
        barpath  = './'+barpath
        debug_info('create_code_bar','INFO','Returned path ->'+barpath)
        return './'+barpath
    except Exception as e:
        debug_info('create_code_bar','EXCEPTION CAUGHT',e)
        return False



## This function creates saves on media folder and renames the assitant image
# @param filename The name that will be assigned to the image
# @param file The image post data
# @return path/False Returns images's full path if success or None if something goes wrong
def handle_uploaded_file(file, filename):

    try:
        if isExtensionAllowed(file) == False:
            raise Exception('Not allowed file extension') 
        path = 'media/upload_images/'

        filename = filename +'.'+file.name.split('.')[1]
        if not os.path.exists(path):
            os.mkdir(path)
     
        with open(path + filename, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
        path = './'+path+filename
        debug_info('handle_uploaded_file','INFO','Returned path ->'+path)
        return path
    except Exception as e:
        debug_info('handle_uploaded_file','EXCEPTION CAUGHT',e)
        return False


## This function saves the assistant on database
# @param values A dictionary with the assitant data from formulary
# @return True/False Returns a True flag if success or False flag if something goes wrong
def save_assistant(values):
    try:
        consejero=Consejeros(id =values['id'], nombre =values['nombre'],facultad=values['facultad'],cargo=values['cargo'],image_path=values['image_path'], barcode_path = values['barcode_path'], suplente = values['suplente'] )
        consejero.save(force_insert=True)
        rfid = Rfid(id_rfid=values['rfid'], id_consejero=consejero)
        rfid.save(force_insert=True)
        debug_info('save_asisstant','INFO','Saved Successfully')
        return True
    except Exception as e:
        debug_info('save_asisstant','EXCEPTION CAUGHT',e)
        return False




## This function checks if a some field in a dictionary is empty
# @param values The Dictionary with fields to check
# @return True/False If founds some empty field or False if not
def CheckEmptyFields(values):
    try:
        value_items= values.values()
        for item in value_items:
            if item == '' or item is None:
                return True
        return False
    except Exception as e:
        debug_info('CheckEmptyFields','EXCEPTION CAUGHT',e)
        return False

## This function get the extension from a file object
# @param file The file object
# @return True/False Returns True if is allowed or False if is not allowed
def isExtensionAllowed(file):
    allowed_ext=['jpeg','png']
    ex=file.content_type.split('/')[1]
    for ext in allowed_ext:
        if ex == ext:
            return True
    return False
