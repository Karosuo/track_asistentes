from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^alta_evento/', views.altas_eventos_form, name = 'altas_eventos_page'),
]