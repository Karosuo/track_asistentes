## @package altas_eventos_app
# This is the controller for the Altas eventos HTML page.
#
# This view is responsible for responding to formulary in the HTML page altas eventos
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from altas_eventos_app.models import Evento
from django.contrib import messages
from django.http import HttpResponseRedirect

# This variable is used to allow messages related to functions debug information
verbose = True

## This function gets the information from the alta evento formulary in the HTML
# @param request The POST object received from HTML form.
# @return View The HTML viewl related to alta evento form
def altas_eventos_form(request):
	if request.method == "GET":
		return render(request,'altas_eventos_templates/alta_evento.html')
	if request.method == "POST":
		if process(request):
			messages.success(request, 'Evento registrado')
		else:
			messages.error(request, 'El evento no pudo ser registrado o ya existe')
		return render(request, 'altas_eventos_templates/alta_evento.html')


## This function is responsible for make all data process and query process
# @param request The POST object received from HTML form.
# @return True/False Return True flag if all is correct or False flag if something fail
def process(request):
	print "\n\n---------- Processing request (ALTAS EVENTOS app) ----------"
	values = get_post_data(request)
	if values == False:
		return False
	if alta_evento(values) == False:
		return False
	else:
		return True


## This function shows debug info from functions
# @param function_name Name of calling function
# @param tag Tag 'ERROR','INFO','EXCEPTION'
# @param message Info Message
# @return None
def debug_info(function_name,tag, message):
    if verbose:
        print'\n'
        print "---------------------------------------------------------------"
        print tag,'in function: ___',function_name,'()___'
        print '\t',tag,':',message
        print "---------------------------------------------------------------"
        print '\n'






## This function gets the request POST object from the form
# and process it in order to get the data related to an event that
# will be register in the database
# @param request The POST object received from HTML form.
# @return Values/False Returns a dictionary with post data if success or False flag if something goes wrong
def get_post_data(request):
	try:
		values = {"numero-evento":request.POST["numero_evento"],
		"fecha-evento":request.POST["fecha"],
		"lugar-evento":request.POST["lugar"]}
		debug_info('get_post_data','EXCEPTION CAUGHT','Dictionary: '+ str(values))
		return values
	except Exception as e:
		debug_info('get_post_data','EXCEPTION CAUGHT',e)
		return False


## This function saves a dictionary with the evento form data
# in the database
# @param values Dictionary with formulary values.
# @return True/False Returns True flag if success or False flag if something goes wrong
def alta_evento(values):
	try:
		# Create an Evento object with the dictionary data
		evento = Evento(numero_evento =values["numero-evento"],fecha = values["fecha-evento"],lugar =values["lugar-evento"])
		# Save the object in the database (force_insert is to force an insert and disables update field)
		evento.save(force_insert=True)
		# Debug info
		debug_info('get_post_data','EXCEPTION CAUGHT','Evento Saved successfully')
		return True
	except Exception as e:
		debug_info('alta_evento','EXCEPTION CAUGHT',e)
		return False