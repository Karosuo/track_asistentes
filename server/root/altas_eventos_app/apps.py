from __future__ import unicode_literals

from django.apps import AppConfig


class AltasEventosAppConfig(AppConfig):
    name = 'altas_eventos_app'
