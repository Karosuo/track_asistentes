## @package consultas_app
# This is the controller for the consultas template.
#
# This view is responsible for make the queries to database with the RFID as a parameter
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from consultas_app.models import Consejeros
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from consultas_app.models import Rfid



# This variable is used to allow messages related to functions debug information
verbose = True


# This tag is required to restrict access to unlogged users
@login_required
## This function is responsible for make the querys to database using a RFID from HTML form
# @param request The POST object received from HTML form with the RFID.
# @return template The template with the response HTML form and assitant requested information
def consulta_form(request):

	if request.method == 'GET':
		return render(request,'consultas_app_templates/consultas.html')

	if request.method == 'POST':

		if request.POST.get("delete_assistant"):
			delete_assistant()
			print "post",request.POST
			messages.success(request,"EXITO: Asistente eliminado")
			return render(request,'consultas_app_templates/consultas.html')

		if request.POST.get("search_assistant"):
			context = process(request)
			if context == False:
				return render(request,'consultas_app_templates/consultas.html')
			else:
				return render(request,'consultas_app_templates/consultas.html',context)


## This function shows debug info from functions
# @param function_name Name of calling function
# @param tag Tag 'ERROR','INFO','EXCEPTION'
# @param message Info Message
# @return None
def debug_info(function_name,tag, message):
    if verbose:
        print "---------------------------------------------------------------"
        print tag,'in function: ___',function_name,'()___'
        print '\t',tag,':',message
        print "---------------------------------------------------------------"

## This function is responsible for make all data process and save process
# for formulary data
# @param request The POST object received from HTML form.
# @return True/False Retrun True flag if all is correct or False flag if something fail
def process(request):
	print "\n\n---------- Processing request (CONSULTAS app) ----------"
	rfid = get_post_data(request)
	if rfid == False:
		messages.error(request, 'ERROR: No se pudo leer el RFID')
		return False
	asisstant = look_for_assistant(rfid)
	if asisstant == False:
		messages.error(request, 'ERROR: No hay ningun asistente asociado al RFID: '+rfid)
		return False
	context = prepare_context(asisstant)
	if context == False:
		messages.error(request, 'ERROR: Algo salio mal con la respuesta')
		return False
	else:
		return context


## This function gets the request POST object from the form
# and process it in order to get the data related to an assistant's RFID
# @param request The POST object received from HTML form.
# @return Values/False Returns a dictionary with post data if success or False flag if something goes wrong
def get_post_data(request):
	try:
		rfid = request.POST.get('rfid')
		debug_info('get_post_data','INFO','Recovered RFID: '+rfid)
		return rfid
	except Exception as e:
		debug_info('get_post_data','EXCEPTION CAUGHT',e)
		return False

## This function looks an assistant related to a RFID given
# @param rfid RFID to perform the search
# @return consejero/False Returns assistant if success or False flag if something goes wrong
def look_for_assistant(rfid):
	try:
		consejero=Consejeros.objects.get(rfid=rfid)
		return consejero
	except Exception as e:
		debug_info('look_for_assistant','EXCEPTION CAUGHT',e)
		return False

## This function creates a dictioanry with an assistan object
# @param assistant Assistant object
# @return context/False Returns dictionary if success or False flag if something goes wrong
def prepare_context(assistant):
	try:
		rfid=Rfid.objects.get(id_consejero = assistant.id)
		context = {
			'rfid': rfid.id_rfid,
		    'id': assistant.id,
		    'nombre': assistant.nombre,
		    'facultad': assistant.facultad,
		    'cargo': assistant.cargo,
		    'suplente': assistant.suplente,
		    'image_path': assistant.image_path,
		    'barcode_path': assistant.barcode_path,
		    'success': True,
		}
		debug_info('prepare_context','INFO','context: '+str(context))
		return context
	except Exception as e:
		debug_info('prepare_context','EXCEPTION CAUGHT',e)
		return False


def delete_assistant():
	try:
		print rfid
	except Exception as e:
		debug_info('delete_assistant','EXCEPTION CAUGHT',e)
	print "Eliminar asistente"
