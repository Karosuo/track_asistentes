# README #

This README would normally document whatever steps are necessary to get your application up and running.

follow the steps in the next link: http://mohiulalamprince.blogspot.mx/2013/06/mysql-python-install-problem-using.html


Track Asistentes
===================


Track Asistentes is a web system in charge of the registration, visualization and attendance of users to an event, the system works in conjunction with devices capable of reading multiple bar codes as well as rfids in badges. The system is designed to automate the whole process of managing the entry and exit to an event.

Getting Started
-------------
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 


Prerequisites
-------------
* Python 2.7.x [download here](https://www.python.org/downloads/)
* Make sure that python is in the system PATH
* MySQL latest version [download here](https://dev.mysql.com/downloads/windows/installer/5.7.html)
* Microsoft Visual C++ Compiler for Python [download here](https://www.microsoft.com/en-us/download/confirmation.aspx?id=44266)
> **Note:**
> Make sure Python is in system PATH 
> [How do it?](http://lingwars.github.io/blog/instalar-python-en-windows-7.html)

Installing
-------------
First it's necessary setup somethings in settings.py, this file is in server/root/track_asistentes folder.

Make sure it looks like this.
```
DATABASES = {
'default': {
'ENGINE': 'django.db.backends.mysql',
'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
'NAME': 'consejo_db',# Name of your database
'USER': 'root', #User of your database
'PASSWORD': '1234', #Password for your database
'HOST': 'localhost' #Address of your database
'PORT': '3306', # Port
}
}
```
Now it's necessary run the db scripts in DataBase folder just copy and paste to MySQL workBench.

Install a virtual enviroment, just type this in your terminal.  [What's this?](http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/)
```sh
$ pip install virtualenv
```
Create a virtual enviroment and activate it, you can skip this if you already have one.

```sh
$ virtualenv name_that_you_want
```
Navigate to the folder created in the previous step and move to the Scripts folder then run the next.

```sh
$ activate
```
Now install django typing the next: [What's this?](https://www.djangoproject.com/)
```sh
$ pip install django
```
We will need install the following libraries:
```sh
$ pip install pillow 
$ pip install pybarcode
$ easy_install mysql-python
```
Libraries description.

* PIllow
Python Imaging Library (PIL) is a free library that allows editing of images directly from Python. It supports a variety of formats, including most used as GIF, JPEG and PNG. A large part of the code is written in C, for performance reasons.

* pybarcode
This library provides a simple way to create barcodes using only the Python standardlib. The barcodes where created as SVG objects.

* mysql-python
This is a connector that let Python connect to MySQL Databases 

Now navigate to root folder, in this folder in this folder are all the modules of the system and django manage.py file, this file contains a set of tools for django.

Run the following:

```sh
$ pip manage.py migrate
```
This command will copy all your models to your database, a model is a file that contains classes that represents tables, you can skip this if you already have the tables updated.

Now it's nessesary create a super user for the django admin site [What's this?](https://docs.djangoproject.com/en/1.11/ref/contrib/admin/)
```sh
$ pip manage.py createsuperuser
```
With this superuser you can create more users and modify or delete the database tables from database, is like admin user.

Finally run the following command:
```sh
$ pip manage.py runserver
```
This command will deploy a development server included in django.
To access to de web site just type this  http://127.0.0.1:8000/login/  on your web browser.

And it's done.

Project folders
-------------
* altas_app
This is the module to register assistants

* altas_eventos_app
This is the module to register events

* consultas_app
This is the module to view assistants registered

* login_app
This module is handles logins is managed by django

* media
In this folder will locate all media files uploaded by users

* static/base
In this folder are all static files using by the templates

* templates
This folder contains all .js,.html abnd .css for all modules

* track_asistentes
This folder contains django configurations files

